Brian Bijdeveldt is a business coach to entrepreneurs and service professionals, author, speaker and trainer. For over 10 years he has provided small business coaching services to business owners.

Website : https://www.profitkoach.com.au/
